<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'users'], function () use ($router) {
    $router->get('/', ['as' => 'users.index', 'uses' => 'UserController@index']);
    $router->post('/{id}/pay', ['as' => 'users.pay', 'uses' => 'UserController@pay']);
    $router->post('/', ['as' => 'users.store', 'uses' => 'UserController@store']);
    $router->get('/{id}', ['as' => 'users.show', 'uses' => 'UserController@show']);
    $router->patch('/{id}', ['as' => 'users.update', 'uses' => 'UserController@update']);
    $router->delete('/{id}', ['as' => 'users.destroy', 'uses' => 'UserController@destroy']);
});
