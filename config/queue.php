<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    |
    */

    'connections' => [
        'rabbitmq' => [
            'host' => env('RABBITMQ_HOST', 'rabbitmq'),
            'port' => env('RABBITMQ_PORT', 5672),
            'username' => env('RABBITMQ_USERNAME', 'root'),
            'password' => env('RABBITMQ_PASSWORD', '123456'),
            'vhost' => env('RABBITMQ_VHOST', 'vhost_podro'),
        ],
    ]
];
