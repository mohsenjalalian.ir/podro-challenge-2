<?php


namespace App\Queues;

class PaymentQueue extends Queue
{
    private const QUEUE_NAME = 'payment';
    private const QUEUE_PERSISTENT_KEY = 'RABBIT_PERSIST_PAYMENT';

    public function __construct()
    {
        $this->__set('queueName', self::QUEUE_NAME);
        $this->__set('queuePersistKey', self::QUEUE_PERSISTENT_KEY);
    }
}
