<?php

namespace App\Queues;

use ErrorException;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

Abstract class Queue
{
    private $queueName;
    private $queuePersistKey;

    /**
     * @return AMQPStreamConnection
     */
    protected function connection()
    {
        return app('rabbitMQ');
    }

    /**
     * @param array $message
     * @param bool $canCloseChannel
     * @param AMQPChannel|null $channel
     * @param array $messageArguments
     * @param bool $passive
     * @param bool $exclusive
     * @param bool $autoDelete
     */
    public function basicPublish(
        array $message,
        bool $canCloseChannel = true,
        AMQPChannel $channel = null,
        array $messageArguments = [],
        bool $passive = false,
        bool $exclusive = false,
        bool $autoDelete = false
    )
    {
        if (count($messageArguments) == 0) {
            $delivery_mode = env($this->__get('queuePersistKey')) ? AMQPMessage::DELIVERY_MODE_PERSISTENT : AMQPMessage::DELIVERY_MODE_NON_PERSISTENT;
            $messageArguments = ['delivery_mode' => $delivery_mode];
        }
        $this->push(
            $this->__get('queueName'),
            json_encode($message),
            env($this->__get('queuePersistKey')),
            $canCloseChannel,
            $channel,
            $messageArguments,
            $passive,
            $exclusive,
            $autoDelete
        );
    }

    /**
     * @param $callback
     * @param string $consumerTag
     * @param bool $noLocal
     * @param bool $noAck
     * @param bool $exclusive
     * @param bool $noWait
     * @throws ErrorException
     */
    public function basicConsume(
        $callback,
        string $consumerTag = '',
        bool $noLocal = false,
        bool $noAck = true,
        bool $exclusive = false,
        bool $noWait = false
    )
    {
        $channel = $this->channel($this->__get('queueName'), env($this->__get('queuePersistKey')));

        $channel->basic_consume($this->__get('queueName'), $consumerTag, $noLocal, $noAck, $exclusive, $noWait, $callback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }
    }

    /**
     * @param AMQPChannel $channel
     * @param bool $noAck
     * @param null $ticket
     * @return mixed
     */
    public function basicGet(AMQPChannel $channel, bool $noAck = true, $ticket = null)
    {
        return $channel->basic_get($this->__get('queueName'), $noAck, $ticket);
    }

    /**
     * @param bool $passive
     * @param bool $exclusive
     * @param bool $autoDelete
     * @return AMQPChannel
     */
    public function getChannel(bool $passive = false, bool $exclusive = false, bool $autoDelete = false): AMQPChannel
    {
        return $this->channel($this->__get('queueName'), env($this->__get('queuePersistKey')), $passive, $exclusive, $autoDelete);
    }

    /**
     * @param $property
     * @return null
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        return null;
    }

    /**
     * @param $property
     * @param $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    /**
     * @param string $queue
     * @param bool $durable
     * @param bool $passive
     * @param bool $exclusive
     * @param bool $autoDelete
     * @return AMQPChannel
     */
    private function channel(
        string $queue,
        bool $durable,
        bool $passive = false,
        bool $exclusive = false,
        bool $autoDelete = false
    ): AMQPChannel
    {
        $channel = $this->connection()->channel();
        $channel->queue_declare($queue, $passive, $durable, $exclusive, $autoDelete);

        return $channel;
    }

    /**
     * @param string $queue
     * @param string $message
     * @param bool $durable
     * @param bool $canCloseChannel
     * @param $channel
     * @param array $messageArguments
     * @param bool $passive
     * @param bool $exclusive
     * @param bool $autoDelete
     */
    private function push(
        string $queue,
        string $message,
        bool $durable,
        bool $canCloseChannel,
        $channel,
        array $messageArguments,
        bool $passive,
        bool $exclusive,
        bool $autoDelete
    )
    {
        if (!$channel) {
            $channel = $this->connection()->channel();
            $channel->queue_declare($queue, $passive, $durable, $exclusive, $autoDelete);
        }
        $message = new AMQPMessage($message, $messageArguments);
        $channel->basic_publish($message, '', $queue);
        if ($canCloseChannel) {
            $channel->close();
        }
    }
}
