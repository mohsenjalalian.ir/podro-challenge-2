<?php


namespace App\Queues;

class UserQueue extends Queue
{
    public const TYPE_CREATE = 'create';
    public const TYPE_DELETE = 'delete';

    private const QUEUE_NAME = 'user';
    private const QUEUE_PERSISTENT_KEY = 'RABBIT_PERSIST_USER';

    public function __construct()
    {
        $this->__set('queueName', self::QUEUE_NAME);
        $this->__set('queuePersistKey', self::QUEUE_PERSISTENT_KEY);
    }
}
