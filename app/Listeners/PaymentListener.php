<?php

namespace App\Listeners;

use App\Models\User;
use App\Queues\PaymentQueue;
use Closure;
use ErrorException;

class PaymentListener
{
    private $paymentQueue;

    /**
     * PaymentDoneEvent constructor.
     * @param PaymentQueue $paymentQueue
     */
    public function __construct(
        PaymentQueue $paymentQueue
    )
    {
        $this->paymentQueue = $paymentQueue;
    }

    /**
     * @throws ErrorException
     */
    public function handle()
    {
        $this->paymentQueue->basicConsume($this->callback());
    }

    /**
     * @return Closure
     */
    public function callback(): Closure
    {
        return function ($message) {
            $message = json_decode($message->body);
            $useId = $message->id;
            $balance = $message->balance;

            User::where('id', $useId)->update(['balance' => $balance]);
        };
    }
}
