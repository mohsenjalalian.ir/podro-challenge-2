<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Validator;

class UserRequest
{
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();
        }
    }

    public function update()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();
        }
    }

    public function pay()
    {
        $validator = Validator::make(request()->all(), [
            'balance' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();
        }
    }
}
