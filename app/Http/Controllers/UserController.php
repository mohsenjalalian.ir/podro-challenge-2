<?php

namespace App\Http\Controllers;

use App\Events\PaymentEvent;
use App\Events\UserDeletedEvent;
use App\Events\UserSavedEvent;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userRequest;

    /**
     * UserController constructor.
     * @param UserRequest $userRequest
     */
    public function __construct(
        UserRequest $userRequest
    )
    {
        $this->userRequest = $userRequest;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $users = User::latest()->paginate();

        return response()->json($users, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param UserSavedEvent $userSavedEvent
     * @return JsonResponse
     */
    public function store(Request $request, UserSavedEvent $userSavedEvent)
    {
        $validator = $this->userRequest->store();

        if ($validator) {
            return response()->json($validator, Response::HTTP_BAD_REQUEST);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        event($userSavedEvent->dispatch($user));

        return response()->json($user, Response::HTTP_CREATED);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $user = User::findorFail($id);

        return response()->json($user, Response::HTTP_OK);
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request)
    {
        $validator = $this->userRequest->update();

        if ($validator) {
            return response()->json($validator, Response::HTTP_BAD_REQUEST);
        }

        $user = User::findOrFail($id);

        $user->update($request->all());

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param $id
     * @param UserDeletedEvent $userDeletedEvent
     * @return JsonResponse
     */
    public function destroy($id, UserDeletedEvent $userDeletedEvent)
    {
        $user = User::findOrFail($id);
        $user->delete();

        event($userDeletedEvent->dispatch($user->id));

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param $id
     * @param Request $request
     * @param PaymentEvent $paymentEvent
     * @return JsonResponse
     */
    public function pay($id, Request $request, PaymentEvent $paymentEvent)
    {
        $validator = $this->userRequest->pay();

        if ($validator) {
            return response()->json($validator, Response::HTTP_BAD_REQUEST);
        }

        $user = User::findOrFail($id);

        event($paymentEvent->dispatch($user->id, $request->get('balance')));

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
