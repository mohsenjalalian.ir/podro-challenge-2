<?php

namespace App\Events;

use App\Queues\UserQueue;

class UserDeletedEvent
{
    private $userQueue;
    private $userQueueChannel;

    /**
     * UserController constructor.
     * @param UserQueue $userQueue
     */
    public function __construct(
        UserQueue $userQueue
    )
    {
        $this->userQueue = $userQueue;
        $this->userQueueChannel = $userQueue->getChannel();
    }

    public function dispatch($userId)
    {
        $this->userQueue->basicPublish(
            [
                'type' => UserQueue::TYPE_DELETE,
                'id' => $userId
            ],
            false,
            $this->userQueueChannel
        );
    }
}
