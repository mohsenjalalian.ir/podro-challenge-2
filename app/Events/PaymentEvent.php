<?php

namespace App\Events;

use App\Queues\PaymentQueue;

class PaymentEvent
{
    private $paymentQueue;
    private $paymentQueueChannel;

    /**
     * PaymentDoneEvent constructor.
     * @param PaymentQueue $paymentQueue
     */
    public function __construct(
        PaymentQueue $paymentQueue
    )
    {
        $this->paymentQueue = $paymentQueue;
        $this->paymentQueueChannel = $paymentQueue->getChannel();
    }

    public function dispatch($userId, $balance)
    {
        $this->paymentQueue->basicPublish(
            [
                'id' => $userId,
                'balance' => $balance,
                'created_at' => time()
            ],
            false,
            $this->paymentQueueChannel
        );
    }
}
