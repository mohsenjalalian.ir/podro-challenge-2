<?php

namespace App\Events;

use App\Queues\UserQueue;

class UserSavedEvent
{
    private $userQueue;
    private $userQueueChannel;

    /**
     * UserController constructor.
     * @param UserQueue $userQueue
     */
    public function __construct(
        UserQueue $userQueue
    )
    {
        $this->userQueue = $userQueue;
        $this->userQueueChannel = $userQueue->getChannel();
    }

    public function dispatch($user)
    {
        $this->userQueue->basicPublish(
            [
                'type' => UserQueue::TYPE_CREATE,
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'created_at' => $user->created_at
            ],
            false,
            $this->userQueueChannel
        );
    }
}
