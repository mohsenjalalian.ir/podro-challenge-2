<?php

namespace App\Providers;

use App\Connections\RabbitMQConnection;
use Illuminate\Support\ServiceProvider;

class RabbitMQServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('rabbitMQ', function () {
            return (new RabbitMQConnection())->connection;
        });
    }
}
