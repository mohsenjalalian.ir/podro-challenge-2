<?php

namespace App\Connections;

use PhpAmqpLib\Connection\AMQPStreamConnection;

class RabbitMQConnection
{
    public $connection;

    /**
     * Connection constructor.
     */
    public function __construct()
    {
        $config = config('queue.connections.rabbitmq');
        $this->connection = new AMQPStreamConnection(
            $config['host'],
            $config['port'],
            $config['username'],
            $config['password'],
            $config['vhost']
        );
    }
}
