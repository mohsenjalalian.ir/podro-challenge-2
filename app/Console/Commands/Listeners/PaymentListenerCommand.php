<?php


namespace App\Console\Commands\Listeners;

use App\Listeners\PaymentListener;
use Illuminate\Console\Command;
use ErrorException;

class PaymentListenerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'podro:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'handle payment done event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param PaymentListener $paymentListener
     * @throws ErrorException
     */
    public function handle(PaymentListener $paymentListener)
    {
        $paymentListener->handle();
    }
}
